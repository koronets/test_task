# Test task

To run the app, run commands:
- php yii serve
- npm run serve

Files to review:
- /frontend/src/views/Home.vue
- /frontend/src/components/ApiTable.vue
- /controllers/BaseApiController.php
- /controllers/NewCarBrandsController.php
