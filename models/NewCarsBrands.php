<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "new_cars_brands".
 *
 * @property int $id
 * @property int $new_car_id
 * @property int|null $parent_car_id
 * @property string|null $brand
 * @property string|null $model_class
 * @property string|null $model_name
 * @property string|null $package_name
 * @property string|null $package_title
 * @property string|null $package_description
 * @property int|null $package_description_split
 * @property int|null $fuel_id
 * @property int|null $fuel2_id
 * @property int|null $fuel_filter_id
 * @property int|null $drive_train_id
 * @property int|null $transmision_id
 * @property string|null $transmision_description
 * @property float|null $engine cc
 * @property string|null $engine_description
 * @property int|null $horse_power
 * @property int|null $tog nm
 * @property int|null $co2
 * @property float|null $consumption
 * @property float|null $accelaration
 * @property int|null $price
 * @property int|null $price_hide
 * @property int|null $check_price
 * @property int|null $discount
 * @property string|null $photo
 * @property string|null $thumbnail
 * @property string|null $big_photo
 * @property string|null $raw_thumbnail
 * @property string|null $raw_big_photo
 * @property string|null $model_page
 * @property string|null $model_pricelist
 * @property int|null $model_year
 * @property float|null $battery_capacity
 * @property string|null $affinity
 * @property int|null $flokkar_id
 * @property string|null $extra_packages
 * @property string|null $color
 * @property string|null $registration_number
 * @property int|null $tags
 * @property int|null $status
 * @property string|null $description
 * @property int|null $disabled
 * @property string|null $created
 * @property string|null $updated
 *
 * @property NewCarsCompany $newCar
 * @property NewCarsCat[] $newCarsCats
 * @property NewCarsPackage[] $newCarsPackages
 */
class NewCarsBrands extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'new_cars_brands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['new_car_id'], 'required'],
            [['new_car_id', 'parent_car_id', 'package_description_split', 'fuel_id', 'fuel2_id', 'fuel_filter_id', 'drive_train_id', 'transmision_id', 'horse_power', 'tog', 'co2', 'price', 'price_hide', 'check_price', 'discount', 'model_year', 'flokkar_id', 'tags', 'status', 'disabled'], 'integer'],
            [['package_description', 'description'], 'string'],
            [['engine', 'consumption', 'accelaration', 'battery_capacity'], 'number'],
            [['created', 'updated'], 'safe'],
            [['brand'], 'string', 'max' => 150],
            [['model_class', 'model_name', 'package_name', 'transmision_description', 'engine_description', 'affinity'], 'string', 'max' => 50],
            [['package_title', 'thumbnail', 'big_photo', 'model_page', 'model_pricelist'], 'string', 'max' => 255],
            [['photo'], 'string', 'max' => 300],
            [['raw_thumbnail', 'raw_big_photo', 'extra_packages', 'color'], 'string', 'max' => 200],
            [['registration_number'], 'string', 'max' => 20],
            [['new_car_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewCarsCompany::className(), 'targetAttribute' => ['new_car_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'new_car_id' => 'New Car ID',
            'parent_car_id' => 'Parent Car ID',
            'brand' => 'Brand',
            'model_class' => 'Model Class',
            'model_name' => 'Model Name',
            'package_name' => 'Package Name',
            'package_title' => 'Package Title',
            'package_description' => 'Package Description',
            'package_description_split' => 'Package Description Split',
            'fuel_id' => 'Fuel ID',
            'fuel2_id' => 'Fuel2 ID',
            'fuel_filter_id' => 'Fuel Filter ID',
            'drive_train_id' => 'Drive Train ID',
            'transmision_id' => 'Transmision ID',
            'transmision_description' => 'Transmision Description',
            'engine' => 'Engine',
            'engine_description' => 'Engine Description',
            'horse_power' => 'Horse Power',
            'tog' => 'Tog',
            'co2' => 'Co2',
            'consumption' => 'Consumption',
            'accelaration' => 'Accelaration',
            'price' => 'Price',
            'price_hide' => 'Price Hide',
            'check_price' => 'Check Price',
            'discount' => 'Discount',
            'photo' => 'Photo',
            'thumbnail' => 'Thumbnail',
            'big_photo' => 'Big Photo',
            'raw_thumbnail' => 'Raw Thumbnail',
            'raw_big_photo' => 'Raw Big Photo',
            'model_page' => 'Model Page',
            'model_pricelist' => 'Model Pricelist',
            'model_year' => 'Model Year',
            'battery_capacity' => 'Battery Capacity',
            'affinity' => 'Affinity',
            'flokkar_id' => 'Flokkar ID',
            'extra_packages' => 'Extra Packages',
            'color' => 'Color',
            'registration_number' => 'Registration Number',
            'tags' => 'Tags',
            'status' => 'Status',
            'description' => 'Description',
            'disabled' => 'Disabled',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * Gets query for [[NewCar]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNewCar()
    {
        return $this->hasOne(NewCarsCompany::className(), ['id' => 'new_car_id']);
    }

    /**
     * Gets query for [[NewCarsCats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNewCarsCats()
    {
        return $this->hasMany(NewCarsCat::className(), ['car_id' => 'id']);
    }

    /**
     * Gets query for [[NewCarsPackages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNewCarsPackages()
    {
        return $this->hasMany(NewCarsPackage::className(), ['car_id' => 'id']);
    }
}
