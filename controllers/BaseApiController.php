<?php

namespace app\controllers;

use yii\web\Controller;

/**
 * Base api controller renders json output for all actions and attaches cors checks
 * @package app\controllers
 */
class BaseApiController extends Controller
{
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ];
    }

    public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return parent::beforeAction($action);
    }
}