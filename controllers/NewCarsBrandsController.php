<?php

namespace app\controllers;

use app\models\NewCarsBrands;

class NewCarsBrandsController extends BaseApiController
{
    public function actionIndex()
    {
        return [
            'data' => NewCarsBrands::find()->all(),
        ];
    }

}
